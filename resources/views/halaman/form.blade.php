@extends('layout.master')

@section('judul')
    Halaman Form
@endsection

@section('content')
    <form action="" method="">
        @csrf
        <label>Nama Lengkap</label><br><br>
        <input type="text" name="name"><br><br>
        <label>Alamat</label><br><br>
        <textarea name="address" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Kirim">
        </form>
@endsection